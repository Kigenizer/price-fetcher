import bs4
import logging
import requests

from term import Atom
from pyrlang.node import Node
from pyrlang.process import Process
from colors import color

LOG = logging.getLogger(color("PRICE_FETCHER_DEMO", fg='orange'))
logging.getLogger("").setLevel(logging.DEBUG)


class PriceFetcherProcess(Process):
    def __init__(self) -> None:
        Process.__init__(self)
        self.get_node().register_name(self, Atom('price_fetcher_process'))
        LOG.info("Process - 'price_fetcher_process' registered!")

    def handle_one_inbox_message(self, msg):
        url = str(msg[0])
        page = requests.get(url)
        parsed_html = bs4.BeautifulSoup(page.content, "html.parser")
        result = []
        prices = parsed_html.find_all('h4', class_='price')
        for price in prices:
            result.append(price.text)
        LOG.info("Prices received from %s: %s", url, result) # For DEMO purposes. But this can be changed by other side operation


def main():
    n = Node(node_name="pricefetcher@127.0.0.1", cookie="SECURE_COOKIE")
    PriceFetcherProcess()
    n.run()


if __name__ == "__main__":
    main()
